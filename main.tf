terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
}

provider "azurerm" {
    # The "feature" block is required for AzureRM provider 2.x.
    # If you're using version 1.x, the "features" block is not allowed.
    version = "~>2.0"
    features {}
}

variable "admin_username" {
    type = string
    description = "Administrator user name for virtual machine"
}

variable "admin_password" {
    type = string
    description = "Password must meet Azure complexity requirements"
}

resource "azurerm_resource_group" "rg" {
  name     = "TFResourceGroup"
  location = "eastus"
}

# Create virtual network
resource "azurerm_virtual_network" "vnet" {
  name                = "TFVnet"
  address_space       = ["10.60.0.0/22"]
  location            = "eastus"
  resource_group_name = azurerm_resource_group.rg.name
}

# Create subnet
resource "azurerm_subnet" "subnet1" {
  name                 = "TFSubnet1"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.60.0.0/24"]
}
resource "azurerm_subnet" "subnet2" {
  name                 = "TFSubnet2"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.60.1.0/24"]
}

# Create public IP
resource "azurerm_public_ip" "publicip1" {
  name                = "TFPublicIP1"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}
resource "azurerm_public_ip" "publicip2" {
  name                = "TFPublicIP2"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsg" {
  name                = "TFNSG"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}


# Create network interface
resource "azurerm_network_interface" "nic1" {
  name                      = "NIC1"
  location                  = "eastus"
  resource_group_name       = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "NIC1Confg"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip1.id
  }
}

# Create network interface
resource "azurerm_network_interface" "nic2" {
  name                      = "NIC2"
  location                  = "eastus"
  resource_group_name       = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "NIC2Confg"
    subnet_id                     = azurerm_subnet.subnet2.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip2.id
  }
}

resource "azurerm_virtual_machine" "vm1" {
  name                  = "TFVM1"
  location              = "eastus"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic1.id]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "myOsDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "TFVM1"
    admin_username = var.admin_username
    admin_password = var.admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}





resource "azurerm_virtual_machine" "vm2" {
  name                  = "TFVM2"
  location              = "eastus"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic2.id]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "myOsDisk2"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  os_profile {
    computer_name  = "TFVM2"
    admin_username = var.admin_username
    admin_password = var.admin_password
    custom_data    = file("./files/winrm.ps1")
  }

  os_profile_windows_config{
    provision_vm_agent = true
    winrm {
      protocol = "http"
    }
    # Auto-Login's required to configure WinRM
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "AutoLogon"
      content      = "<AutoLogon><Password><Value>${var.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_username}</Username></AutoLogon>"
    }

    # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "FirstLogonCommands"
      content      = file("./files/FirstLogonCommands.xml")
    }

  }
  
  connection {
    host     = azurerm_public_ip.publicip2.ip_address
    type     = "winrm"
    port     = 5985
    https    = false
    timeout  = "10m"
    user     = var.admin_username
    password = var.admin_password
  }
  provisioner "file" {
    source      = "files/config.ps1"
    destination = "C:/terraform/config.ps1"
    
  }

  provisioner "remote-exec" {
    inline = [
      "powershell.exe -ExecutionPolicy Bypass -File C:/terraform/config.ps1",
    ]
  }


  
  provisioner "local-exec" {
    command = "terraform output -json > ip.json; export ANSIBLE_HOST_KEY_CHECKING=False; export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES; ansible-playbook -i ./hosts ./installjava.yml"
  }

}
output "public_ip_address_unix" {
  value = azurerm_public_ip.publicip1.ip_address
}

output "public_ip_address_win" {
  value = azurerm_public_ip.publicip2.ip_address
}


